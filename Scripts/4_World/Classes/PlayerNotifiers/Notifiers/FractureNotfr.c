//	Created: 16.12.2019
//	Last modified: 
//	Modified by: 
//	Author: Adrian Radzikowski
//	Mail: dgtiarov@protonmail.com

class FractureNotfr: NotifierBase
{
	void FractureNotfr(NotifiersManager manager)
	{
		m_Active = false; 
	}

	override int GetNotifierType()
	{
		return eCustomNotifiers.NTF_FRACTURE;
	}

	override void DisplayBadge()
	{
		
		DisplayElementBadge DisplayElement = DisplayElementBadge.Cast(GetVirtualHud().GetElement(eDisplayElements.DELM_BADGE_FRACTURE));
		
		if( DisplayElement )
		{
			DisplayElement.SetLevel(eBadgeLevel.FIRST);
		}
	}

	override void HideBadge()
	{
		
		DisplayElementBadge DisplayElement = DisplayElementBadge.Cast(GetVirtualHud().GetElement(eDisplayElements.DELM_BADGE_FRACTURE));
		
		if( DisplayElement )
		{
			DisplayElement.SetLevel(eBadgeLevel.NONE);
		}
	}
}