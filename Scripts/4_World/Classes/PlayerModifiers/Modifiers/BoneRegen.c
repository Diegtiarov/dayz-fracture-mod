//	Created: 18.12.2019
//	Last modified: 23.12.2019
//	Modified by: Adrian Radzikowski
//	Author: Adrian Radzikowski
//	Mail: dgtiarov@protonmail.com

modded class BoneRegenMdfr
{
	private const float	MINIMAL_WATER_TO_REGENRATE = 2000;
	private const float	MINIMAL_ENERGY_TO_REGENRATE = 4000;
	private const float BONE_HEALTH_INCREMENT_PER_SEC = 0.5;

	override void Init()
	{
		m_TrackActivatedTime = true;
		m_IsPersistent = true;
		m_ID 					= eModifiers.MDF_BONE_REGEN;
		m_TickIntervalInactive 	= DEFAULT_TICK_TIME_INACTIVE;
		m_TickIntervalActive 	= DEFAULT_TICK_TIME_ACTIVE;
	}

	override protected bool ActivateCondition(PlayerBase player)
	{
		if ( player.GetStatWater().Get() >= MINIMAL_WATER_TO_REGENRATE && player.GetStatEnergy().Get() >= MINIMAL_ENERGY_TO_REGENRATE )
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	override void OnReconnect(PlayerBase player)
	{
		
	}

	override protected void OnActivate(PlayerBase player)
	{
		
	}

	override protected void OnDeactivate(PlayerBase player)
	{
		
	}

	override protected bool DeactivateCondition(PlayerBase player)
	{
		if (  player.GetStatWater().Get() < MINIMAL_WATER_TO_REGENRATE && player.GetStatEnergy().Get() < MINIMAL_ENERGY_TO_REGENRATE )
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	override void OnTick(PlayerBase player, float deltaT)
	{	
		player.AddBoneHealth(BONE_HEALTH_INCREMENT_PER_SEC * deltaT);
	}
}
